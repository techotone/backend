FROM node:lts-alpine3.14
WORKDIR /app
COPY package*.json ./
RUN npm install
EXPOSE 9000
COPY . .
# CMD ["npm", "run", "start"]
CMD ["sh","-c","npm uninstall bcrypt && npm install bcrypt && npm run start"]