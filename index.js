const express = require("express");
const app = express();
const cors = require("cors");
const http = require("http");
const { initializeScheme } = require("./src/database/schema");
const logger = require("./src/utils/logger");

const { WSServer } = require("./src/wsserver/WSServer");
const servicePort = process.env.SVC_PORT || 9000;

let wsServer;
initializeScheme().then((error) => {
  if (error) logger.error(error);
  else {
    app.use(cors());

    let sserver = http.createServer();
    //mount the express server on created server
    sserver.on("request", app);
    wsServer = new WSServer(sserver);

    require("./src/startup/startup")(app);
    require("./src/startup/routes")(app);
    //start the server

    module.exports.wsServer = wsServer;
    sserver.listen(servicePort, (err) => {
      if (err) logger.error("failed to start the server at port" + servicePort);
      else logger.info("http and websocket servers started at port " + servicePort);
    });
  }
});
