const logger = require("../utils/logger");
const { SERVER_ERROR } = require("../utils/statusCode");
const { BaseError, SQLError } = require("./BaseError");

const handleError = (error, res) => {
  if (error instanceof BaseError) {
    logger.error(error.desc);
    return res.status(error.statusCode).send(error.desc);
  } else {
    console.log(error);
    return res.status(SERVER_ERROR).send();
  }
};

module.exports.handleError = handleError;
