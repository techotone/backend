const express = require("express");
const { validate } = require("../models/login");
const { handleError } = require("../error/errorHandler");
const { BadRequest, NotAllowed } = require("../error/BaseError");
const router = express.Router();

const { db } = require("../database/schema");
const jtoken = require("jsonwebtoken");
const config = require("config");
const { compare } = require("bcrypt");
const { OK } = require("../utils/statusCode");

const User = db.User;
const Login = db.Login;
const Role = db.Role;

const tokenKey = config.get("tokenKey");

/**
 * hanldles logins and record logins
 */

router.post("/", async (req, res) => {
  try {
    const sourceAddress = req.socket.remoteAddress;

    const error = await validate(req.body);
    if (error) throw new BadRequest(error);

    //check whether a user exist
    const user = await User.findOne({ where: { UserName: req.body.UserName } });
    if (!user) throw new NotAllowed("Invalid user name or password");

    //check whether is this the initial login
    let login = await Login.findOne({ where: { UserId: user.dataValues.id } });

    // if (!login) {
    //   logger.warn("re redirecting the user to change password");
    //   return res.redirect("changePassowrd");
    // }

    const compResult = await compare(req.body.Password, user.dataValues.Password);

    if (!compResult) throw new NotAllowed("Invalid user name or password");

    //login is valid . so we generate the token , so we get the user's role
    const role = await Role.findOne({ where: { id: user.dataValues.RoleId } });
    const payload = {
      userID: user.dataValues.id,
      role: role.dataValues.Role,
      name: user.dataValues.UserName,
    };

    let token = jtoken.sign(payload, tokenKey, { expiresIn: "100 days" });

    //store the login data
    login = await Login.build({
      UserId: user.dataValues.id,
      SourceIP: sourceAddress,
    });

    await login.save();
    res.status(OK).send({ token });
  } catch (error) {
    handleError(error, res);
  }
});

module.exports = router;
