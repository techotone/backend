const express = require("express");
const router = express.Router();

const { validate } = require("../models/vendor");
const { BadRequest, SQLError, ServerError, NotFound, Conflict } = require("../error/BaseError");
const { generateRandom, deriveKeyFromAdminLogin, decryptAESHEX } = require("../utils/crypto");
const { handleError } = require("../error/errorHandler");
const logger = require("../utils/logger");
const { db } = require("../database/schema");
const { encryptAESHEX } = require("../utils/crypto");
const { OK } = require("../utils/statusCode");

const APP_SECRET_LEN = 20;

const User = db.User;
const Device = db.Device;
const Vendor = db.Vendor;

/**handles creating a new vendor
 *
 */
router.post("/", async (req, res) => {
  let vendor = req.body;

  logger.info("creating a vendor..");
  try {
    const error = await validate(vendor);
    if (error) throw new BadRequest(error);

    //generate the application secret
    let appSecret = generateRandom(APP_SECRET_LEN);

    const key = await deriveKeyFromAdminLogin(User);
    if (!key) throw new ServerError("Application key generation error");

    //console.log("app secret", appSecret);
    //encrypt the application secretn using the key with AES
    const encryptedAppSecret = encryptAESHEX(key, appSecret);
    //console.log("enctypted " + encryptedAppSecret);

    // const decryptedAppSecret = decryptAESHEX(key, encryptedAppSecret);
    // console.log("decrypted " + decryptedAppSecret);

    vendor.ApplicationSecret = encryptedAppSecret;

    try {
      //create the new vendor
      vendor = await Vendor.build(vendor);
      await vendor.save();
    } catch (ex) {
      throw new SQLError(ex.errors[0].message);
    }

    vendor = await getLastVendor();

    const { dataValues } = vendor[0];
    delete dataValues.createdAt;
    delete dataValues.updatedAt;
    delete dataValues.ApplicationSecret;

    res.status(OK).send(vendor);
    //encrypt using
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handles listing all vendors
 */
router.get("/", async (req, res) => {
  try {
    let vendors = await Vendor.findAll();

    vendors.map((vendor) => {
      const { dataValues } = vendor;
      delete dataValues.createdAt;
      delete dataValues.updatedAt;
      delete dataValues.ApplicationSecret;
    });
    res.status(OK).send(vendors);
  } catch (ex) {
    handleError(ex, res);
  }
});

/**
 * hanldes listing a selected vendro
 */

router.get("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const vendor = await Vendor.findOne({ where: { id: id } });
    if (!vendor) throw new NotFound("no vendor found with id " + id);

    const { dataValues } = vendor;

    delete dataValues.createdAt;
    delete dataValues.updatedAt;
    delete dataValues.ApplicationSecret;
    res.status(OK).send(vendor);
  } catch (ex) {
    handleError(ex, res);
  }
});

/**
 * handle udpating a vendor
 */
router.put("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const vendor = await Vendor.findOne({ where: { id: id } });
    if (!vendor) throw new NotFound("no vendor found with id " + id);

    const error = await validate();
    if (error) throw new BadRequest(error);

    await vendor.update(req.body);
    await vendor.save();

    res.status(OK).send(req.body);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handle deleting a vendor
 */
router.delete("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const vendor = await Vendor.findOne({ where: { id: id } });
    if (!vendor) throw new NotFound("no vendor found with id " + id);

    //check if any device has been attached to the vendor
    const device = await Device.findOne({ where: { vendorId: id } });
    if (device)
      //there is deivce attached so we must abort deleting
      throw new Conflict("devices had been attached to this vendor, abort deleting");

    //ok proceed for delete
    await vendor.destroy();

    res.status(OK).send();
  } catch (error) {
    handleError(error, res);
  }
});

async function getLastVendor() {
  const vendor = await Vendor.findAll({ limit: 1, order: [["id", "DESC"]] });
  return vendor;
}
module.exports = router;
