const express = require("express");
const router = express.Router();
const { NotFound, BadRequest, ServerError } = require("../error/BaseError");
const { handleError } = require("../error/errorHandler");
const { db } = require("../database/schema");
const { deriveKeyFromAdminLogin, decryptAESHEX, xorHexString } = require("../utils/crypto");
const { OK } = require("../utils/statusCode");
const { ConnectionState } = require("../wsserver/conState");
const logger = require("../utils/logger");

const Device = db.Device;
const Bank = db.Bank;
const User = db.User;
const Key = db.Key;
const KeyInjection = db.KeyInjection;
/**
 * handles delivering the connected terminals to the ws server
 */
router.get("/connected", async (req, res) => {
  try {
    console.log("attemping to get connected devices");

    //attempt to get the connected terminals
    const { wsServer } = require("../../index");
    const terminals = wsServer.getConnectedTerminals();
    if (terminals.length == 0) throw new NotFound("no connected terminals found");

    res.status(200).send(terminals);
  } catch (ex) {
    handleError(ex, res);
  }
});

/**
 * handles injecting the terminals which are selected by client
 */
router.post("/inject", async (req, res) => {
  try {
    //perform basic validation, check each terminal id is valid
    let deviceArray = req.body;

    if (!deviceArray) throw new BadRequest("no device data found");

    //check whethre a pin key has been set for the primary bank
    let bank = await Bank.findOne({ where: { Primary: true } });
    if (!bank) throw new BadRequest("primary bank has not defined");

    //check whether both key component exist
    let keys = await Key.findAll({ where: { BankId: bank.dataValues.id } });
    if (!keys) throw new BadRequest("no keys have been set for the primary bank");

    if (keys.length != 2)
      throw new BadRequest("both key components have not beed defined for the primary bank");

    let comp1 = keys[0].dataValues.Data;
    let comp2 = keys[1].dataValues.Data;

    const keyId = keys[0].dataValues.id;

    //decrypt the key components using admin derived loging key
    const decryptionKey = await deriveKeyFromAdminLogin(User);

    console.log("encrypted comp1 ", comp1);
    console.log("encrypted comp2 ", comp2);

    comp1 = decryptAESHEX(decryptionKey, comp1);
    comp2 = decryptAESHEX(decryptionKey, comp2);

    console.log("decrypted comp1 ", comp1);
    console.log("decrypted comp2 ", comp2);

    const resultantPrimaryBankKey = xorHexString(comp1, comp2);
    if (!resultantPrimaryBankKey) throw new ServerError("internal error");

    let resultantSecondaryBankKey;

    //get the secondary bank keys
    bank = await Bank.findOne({ where: { Primary: false } });
    if (bank) {
      //check whether both key component exist
      keys = await Key.findAll({ where: { BankId: bank.dataValues.id } });
      if (keys) {
        if (keys.length == 2) {
          let comp1 = keys[0].dataValues.Data;
          let comp2 = keys[1].dataValues.Data;

          //decrypt the key components using admin derived loging key
          const decryptionKey = await deriveKeyFromAdminLogin(User);

          comp1 = decryptAESHEX(decryptionKey, comp1);
          comp2 = decryptAESHEX(decryptionKey, comp2);

          resultantSecondaryBankKey = xorHexString(comp1, comp2);
        }
      }
    }
    console.log("primary key xored ", resultantPrimaryBankKey);
    console.log("secondary key xored ", resultantSecondaryBankKey);

    //construct the key object
    const tmpKeyData = {
      primary: resultantPrimaryBankKey,
      secondary: resultantSecondaryBankKey,
    };

    //validate each terminal
    const terminalsValid = true;

    deviceArray.forEach(async (item) => {
      const sNo = item.SerialNo;
      const device = await Device.findOne({ where: { SerialNo: sNo } });
      if (!device) {
        terminalsValid = false;
        return;
      }
    });

    if (!terminalsValid) throw new BadRequest("one of the temrinal is not valid, aborting");

    //attemp to inject the key for each device
    const { wsServer } = require("../../index");
    deviceArray.forEach(async (device) => {
      const SerialNo = device.SerialNo;
      device.state = wsServer.sendKeyToTerminal(SerialNo, tmpKeyData);
      if (device.state == ConnectionState.NOT_CONNECTED || device.state == ConnectionState.BUSY)
        device.marked = true;
    });

    logger.info("waiting for the result");
    await waitForKeyResult(deviceArray);

    //write the key injection status in to the table for report generation
    console.log("user", req.user);
    const userId = req.user.userID;

    deviceArray.forEach(async (device) => {
      delete device.marked;
      const dev = await wsServer.getConnectedDevice(device.SerialNo);

      if (device.state == ConnectionState.ACK_RECIEVED_FAILED) injectionStatus = "INJECTION FAILED";
      else if (device.state == ConnectionState.ACK_RECIEVED_SUCCESS)
        injectionStatus = "INJECTION SUCCESS";
      else if (device.state == ConnectionState.NOT_CONNECTED)
        injectionStatus = "DEVICE DISCONNECTED";
      else injectionStatus = "UNKNOWN";

      const deviceId = device.id;

      //write the table
      const keyInjection = await KeyInjection.build({
        UserId: userId,
        KeyId: keyId,
        DeviceId: deviceId,
        InjectionState: injectionStatus,
      });
      await keyInjection.save();

      if (dev) dev.setState(ConnectionState.IDLE);
    });

    res.status(OK).send(deviceArray);
  } catch (ex) {
    handleError(ex, res);
  }
});

async function waitForKeyResult(deviceList) {
  const waitTime = 10; //seconds
  const checkInterval = 500; //ms
  let currentLapsed = 0;

  return new Promise((resolve) => {
    const handle = setInterval(() => {
      let ran = false;

      deviceList.forEach((device) => {
        if (!device.marked) {
          ran = true;

          //get the device from the connection map
          const { wsServer } = require("../../index");

          let connectedDevice = wsServer.getConnectedDevice(device.SerialNo);

          if (
            connectedDevice.state == ConnectionState.ACK_RECIEVED_SUCCESS ||
            connectedDevice.state == ConnectionState.ACK_RECIEVED_FAILED
          ) {
            device.state = connectedDevice.state;
            device.marked = true;
          }
        }
      });

      if (!ran) {
        //done for all devices so we return
        clearInterval(handle);
        return resolve(true);
      }

      currentLapsed += checkInterval;
      //console.log("currentlapsed ", currentLapsed);

      if (currentLapsed >= waitTime * 1000) {
        console.log("time out");

        clearInterval(handle);
        return resolve(true);
      }
    }, checkInterval);
  });
}

//retrive all key injections
router.get("/", async (req, res) => {
  try {
    const keyInjections = await KeyInjection.findAll();

    keyInjections.forEach((injection) => {
      delete injection.dataValues.KeyId;
    });

    res.status(OK).send(keyInjections);
  } catch (ex) {
    handleError(ex, res);
  }
});

module.exports = router;
