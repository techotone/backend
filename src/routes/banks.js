const express = require("express");
const { validate } = require("../models/bank");
const { handleError } = require("../error/errorHandler");
const { BadRequest, Conflict, SQLError, NotFound } = require("../error/BaseError");
const { db } = require("../database/schema");
const { OK } = require("../utils/statusCode");
const router = express.Router();

const Bank = db.Bank;
const User = db.User;
/**
 * handles creating a bank
 */

router.post("/", async (req, res) => {
  try {
    const error = await validate(req.body);
    if (error) throw new BadRequest(error);

    //the rule emphasize there only can be 2 banks in total so count the banks
    let banks = await Bank.findAll();
    if (banks.length == 2)
      throw new Conflict("primary and secondary banks have been defined already");

    if (banks.length > 0) {
      const primary = banks[0].dataValues.Primary;
      if (primary && req.body.Primary) throw new Conflict("primary bank  has been defined already");

      if (!primary && !req.body.Primary)
        throw new Conflict("secondary bank has been defined already");
    }

    let bank;
    try {
      //create the new bank
      bank = await Bank.build(req.body);
      await bank.save();
    } catch (error) {
      throw new SQLError(error.errors[0].message);
    }

    bank = await getLastRecord();
    res.status(OK).send(bank);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handles updating a bank
 */
router.put("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    //check the bank existence
    let bank = await Bank.findOne({ where: { id: id } });
    if (!bank) throw new NotFound("bank does not exist with id " + id);

    const error = await validate(req.body);
    if (error) throw new BadRequest(error);

    if (bank.dataValues.Primary && !req.body.Primary) {
      //check whether there already a primary bank
      bank = await Bank.findOne({ where: { Primary: false } });
      if (bank)
        throw new Conflict(
          "can not change the primory state to secondary ,since a secondary bank has already been defined"
        );
    }

    if (!bank.dataValues.Primary && req.body.Primary) {
      //check whether there already a primary bank
      bank = await Bank.findOne({ where: { Primary: true } });
      if (bank)
        throw new Conflict(
          "can not change the secondary state to primary ,since a primary bank has already been defined"
        );
    }

    try {
      bank = await bank.update(req.body);
      await bank.save();
    } catch (error) {
      throw new SQLError(error.errors[0].message);
    }
    bank.id = id;
    res.status(OK).send(bank);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 *
 * handles listing a bank
 */
router.get("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    const bank = await Bank.findOne({ where: { id: id } });
    if (!bank) throw new NotFound("A bank does not exist with id " + id);
    res.status(OK).send(bank);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 *
 * hanlde listing banks
 */
router.get("/", async (req, res) => {
  try {
    const banks = await Bank.findAll();
    if (banks.length == 0) throw new NotFound("no banks found");

    deleteTimeStamps(banks);
    res.status(OK).send(banks);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 *
 * handles deleting a bank
 */
router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  const bank = await Bank.findOne({ where: { id: id } });

  try {
    if (!bank) throw new NotFound("a bank does not exist with id " + id);

    //check whether users have been assigned to the bank
    const user = await User.findOne({ where: { BankId: id } });
    if (user) throw new Conflict("This bank has users assigned ,delete aborted");

    await bank.destroy();
    res.status(OK).send();
  } catch (error) {
    handleError(error, res);
  }
});

const getLastRecord = async () => {
  const bank = await Bank.findAll({ limit: 1, order: [["id", "DESC"]] });
  return bank;
};

function deleteTimeStamps(object) {
  delete object.createdAt;
  delete object.updatedAt;
}

module.exports = router;
