const express = require("express");
const router = express.Router();

const { db } = require("../database/schema");
const { validate } = require("../models/key");
const { handleError } = require("../error/errorHandler");
const {
  BadRequest,
  Forbidden,
  NotAuthorized,
  NotAllowed,
  Conflict,
  NotFound,
} = require("../error/BaseError");
const { OK } = require("../utils/statusCode");
const { Op } = require("sequelize");
const { deriveKeyFromAdminLogin, encryptAESHEX } = require("../utils/crypto");

const User = db.User;
const Key = db.Key;

/**
 * handles creating a key
 */
router.post("/", async (req, res) => {
  try {
    const error = await validate(req.body);
    if (error) throw new BadRequest(error);

    //perform the hex validation
    let data = req.body.Data;
    if (!isHexKey(data)) throw new BadRequest("key data contains invalid data, hex key expected");

    //check whether the request come from key manager
    let user = req.user;
    let userId = user.userID;

    if (user.role != "Key Manager") throw new NotAuthorized("you are not authoized to create keys");

    //check whether the particular user is belong to a bank
    user = await User.findOne({
      where: { id: userId, BankId: { [Op.ne]: null } },
    });

    if (!user) throw new NotAllowed("you are not assinged to a bank. contact administrator");

    //check whether the bounded bank already has the key which is attempted to create, bacause
    //only 1 pin enc key is allowed per bank (2 components)
    const bankID = user.dataValues.BankId;
    userId = user.dataValues.id;

    let key = await Key.findOne({
      where: { BankId: bankID, UserId: userId },
    });

    if (key)
      throw new Conflict("a key component  has already been created for the bank your bounded");

    //check if there some one else has added  a key with same component
    key = await Key.findOne({ where: { BankId: bankID, Component: req.body.Component } });
    if (key)
      throw new Conflict(
        "a same type of key has already been created by one of key manager other than you"
      );

    const tmp = { ...req.body };
    tmp.BankId = bankID;

    //encrypt the key using prim administrator login
    const derivedKey = await deriveKeyFromAdminLogin(User);
    data = encryptAESHEX(derivedKey, data);

    tmp.UserId = userId;
    tmp.Data = data;

    key = await Key.build(tmp);
    await key.save();

    //get the last record
    key = await getLastKey();

    res.status(OK).send(key);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handles listing all key
 */
router.get("/", async (req, res) => {
  try {
    //get the requesting user
    const { userID } = req.user;

    //check if there any key which is created by this user
    const keys = await Key.findAll({ where: { UserId: userID } });
    if (!keys) throw new NotFound("no keys found created by this user");

    //send the found keys after deleting the app secret
    keys.map((key) => {
      delete key.dataValues.createdAt;
      delete key.dataValues.updatedAt;
    });

    res.status(OK).send(keys);
  } catch (ex) {
    handleError(ex, res);
  }
});

/**
 * handles deletion of a selected key
 */
router.delete("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    const key = await Key.findOne({ where: { id: id } });
    if (!key) throw new NotFound("no key found with id " + id);

    const currentUserId = req.user.userID;
    if (key.dataValues.UserId !== currentUserId)
      throw new NotAllowed("you are not allowed to delete this key, because owner is different");

    //delete the key
    await key.destroy();
    res.status(OK).send();
  } catch (ex) {
    handleError(ex, res);
  }
});

async function getLastKey() {
  const keyRecord = await Key.findAll({ limit: 1, order: [["id", "DESC"]] });
  return keyRecord;
}

const hexArray = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
];
const isHexKey = (key) => {
  for (let i = 0; i < key.length; i++) {
    let isHex = false;
    const char = key[i];
    for (let j = 0; j < hexArray.length; j++) {
      if (char == hexArray[j]) {
        isHex = true;
        break;
      }
    }
    if (!isHex) return false;
  }
  return true;
};
module.exports = router;
