const express = require("express");
const router = express.Router();

const { validate } = require("../models/user");
const { db } = require("../database/schema");
const { OK } = require("../utils/statusCode");

const {
  BadRequest,
  BaseError,
  SQLError,
  NotFound,
  Conflict,
  NotAllowed,
} = require("../error/BaseError");
const { handleError } = require("../error/errorHandler");
const { hash, generateRandom } = require("../utils/crypto");
const logger = require("../utils/logger");

const User = db.User;
const Key = db.Key;
const Role = db.Role;
const Bank = db.Bank;

/**
 * handles creating a new user in the sytem
 */
router.post("/", async (req, res) => {
  let user = req.body;

  try {
    if (!user) throw new BadRequest("body can not be empty");

    //validate the user object
    const error = await validate(user);
    if (error) throw new BadRequest(error);

    //generate a rendom password for initial login , this password must be notified to the user via an email
    let randPassword = generateRandom(8);

    //for testing the passowrd is set to 000000
    randPassword = "00000000";
    logger.info("generated random password " + randPassword);

    //set up the hashed password
    const hashedPassword = await hash(randPassword);
    user.Password = hashedPassword;

    //check the role and bank ids are proper
    const role = await Role.findOne({ where: { id: user.RoleId } });
    if (!role) throw new BadRequest("There is no such a role with role id " + user.RoleId);

    if (user.BankId) {
      const bank = await Bank.findOne({ where: { id: user.BankId } });
      if (!bank) throw new BadRequest("There is no such a bank with the bank id " + user.BankId);

      //only key managers are allowed to add for secondary bank users
      if (bank.dataValues.Primary == 0 && role.dataValues.Role != "Key Manager")
        throw new NotAllowed(
          "Users with role type " + role.dataValues.Role + " are not allowed to add secondary banks"
        );
    }

    user.Owner = req.user.userID;
    //ok to create a new user
    try {
      user = await User.build(user);
      await user.save();
    } catch (error) {
      console.log(error);
      throw new SQLError(error.errors[0].message);
    }

    user = await getLatestUser();

    //omit the password in response
    delete user[0].dataValues.Password;
    return res.status(OK).send(user);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * hanldes updating a user
 */
router.put("/:id", async (req, res) => {
  let user;
  try {
    //check whether the particular user exist
    const id = req.params.id;
    user = await User.findOne({ where: { id: id } });
    if (!user) throw new NotFound("a user does not exist with id " + id);

    //check whether the initiation owns the account
    if (req.user.userID != user.dataValues.Owner)
      throw new Conflict("you are not allowed to update this account since you are not the owner");

    const error = await validate(req.body);
    if (error) throw new BadRequest(error);

    //check the role and bank ids are proper
    const role = await Role.findOne({ where: { id: user.RoleId } });
    if (!role) throw new BadRequest("There is no such a role with role id " + user.RoleId);
    if (user.BankId) {
      const bank = await Bank.findOne({ where: { id: user.BankId } });
      if (!bank) throw new BadRequest("There is no such a bank with the bank id " + user.BankId);
    }

    try {
      //copy the target object
      let tmpUser = { ...req.body };
      await user.update(tmpUser);
      await user.save();
    } catch (error) {
      throw new SQLError(error.errors[0].message);
    }

    delete user.dataValues.Password;
    res.status(OK).send(user);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 *
 * handles listing users
 */
router.get("/", async (req, res) => {
  const users = await User.findAll();

  for (let i = 0; i < users.length; i++) {
    delete users[i].dataValues.Password;
  }

  res.status(OK).send(users);
});

/**
 * handles listing a specific user
 * @returns
 */
router.get("/:id", async (req, res) => {
  const id = req.params.id;
  const user = await User.findOne({ where: { id: id } });
  try {
    if (!user) throw new NotFound("user does not exist with id " + id);
  } catch (error) {
    handleError(error, res);
  }

  delete user.dataValues.Password;
  res.status(OK).send(user);
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  const user = await User.findOne({ where: { id: id } });

  try {
    if (!user) throw new NotFound("user does not exist with id " + id);

    //deletion not allowed , if the account belongs to current user
    const currentUserId = req.user.userID;
    if (currentUserId == id) throw new Conflict("you are not allowed to delete your own account");
    else {
      if (currentUserId !== user.dataValues.Owner)
        throw new Conflict("you are not allowed to delete , beacause you are not the owner");
    }

    //check if any key has been bound to the user
    const keysBoundToUser = await Key.findOne({ where: { UserID: id } });
    if (keysBoundToUser) throw new Conflict("This user have associated key, delete aborted");

    await user.destroy();
  } catch (error) {
    handleError(error, res);
  }

  res.status(OK).send();
});

const getLatestUser = async () => {
  const user = await User.findAll({ limit: 1, order: [["id", "DESC"]] });
  return user;
};
module.exports = router;
