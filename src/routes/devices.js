const express = require("express");
const { BadRequest, SQLError, NotFound } = require("../error/BaseError");
const { handleError } = require("../error/errorHandler");
const { validate } = require("../models/device");
const router = express.Router();

const { db } = require("../database/schema");
const { OK } = require("../utils/statusCode");
const Device = db.Device;
const Vendor = db.Vendor;

async function getLastNDevices(numDevices) {
  const devices = await Device.findAll({ limit: numDevices, order: [["id", "DESC"]] });
  return devices;
}
/**
 * facilitetes creating a device/s , the routine expects a json array of device objects
 */
router.post("/", async (req, res) => {
  const deviceArray = req.body;
  try {
    if (!deviceArray.length) throw new BadRequest("no device deta found");

    //validate each device data object
    for (let i = 0; i < deviceArray.length; i++) {
      let device = deviceArray[i];
      const error = await validate(device);
      if (error) throw new BadRequest(error);

      //create each device in the database
      try {
        device = await Device.build(device);
        await device.save();
      } catch (error) {
        throw new SQLError(error.errors[0].message + "\ndevice data = " + JSON.stringify(device));
      }

      const devices = await getLastNDevices(deviceArray.length);
      res.status(OK).send(devices);
    }
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * hanles updataing a device
 */
router.put("/:id", async (req, res) => {
  let device = req.body;

  try {
    const error = await validate(device);
    if (error) throw new BadRequest(error);

    const id = req.params.id;
    device = await Device.findOne({ where: { id: id } });
    if (!device) throw new NotFound("no device found with id " + id);

    //check the provided vendor id is valid
    const vendor = await Vendor.findOne({ where: { id: req.body.VendorId } });
    if (!vendor) throw new NotFound("vendor is not found with id " + req.body.VendorId);

    //we found the device update it
    device = await device.update(req.body);
    await device.save();

    res.status(OK).send(device);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handles retrieving a device
 */
router.get("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    const device = await Device.findOne({ where: { id: id } });
    if (!device) throw new NotFound("device not found with id " + id);
    res.status(OK).send(device);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handles listing all devices
 */
router.get("/", async (req, res) => {
  const devices = await Device.findAll();

  try {
    if (devices.length == 0) throw new NotFound("no devices found");
    res.status(OK).send(devices);
  } catch (error) {
    handleError(error, res);
  }
});

/**
 * handle deleting a device
 */

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  const device = await Device.findOne({ where: { id: id } });

  try {
    if (!device) throw new NotFound("no device found with id " + id);
    await device.destroy();

    res.status(OK).send();
  } catch (error) {
    handleError(error, res);
  }
});

module.exports = router;
