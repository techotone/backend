const ConnectionState = Object.freeze({
  WAITING_FOR_KEY_ACK: 1,
  IDLE: 2,
  BUSY: 3,
  NOT_CONNECTED: 4,
  ACK_RECIEVED_SUCCESS: 5,
  ACK_RECIEVED_FAILED: 6,
  TIME_OUT: 7,
});

module.exports.ConnectionState = ConnectionState;
