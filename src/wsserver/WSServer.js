const logger = require("../utils/logger");
const ClientConnection = require("./ClientConnection");
const HashMap = require("hashmap");
const { WebSocketServer } = require("ws");
const { ConnectionState } = require("./conState");
class WSServer {
  constructor(server) {
    this.server = server;
    this.webSocketServer = new WebSocketServer({ server: this.server });

    this.connectionMap = new HashMap();
    this.init();
  }

  ProtocolMessage = {
    SEND_TID_SNO: "send-sno-tid",
    INSTALL_KEY: "install-key",
  };

  init() {
    this.webSocketServer.on("connection", (connection, request) => {
      logger.info("connection initiated from ", request.socket.remoteAddress);
      connection.isAlive = true; //to implement ping pong
      connection.on("pong", () => {
        connection.isAlive = true;
      });

      connection.messageCount = 0;

      connection.on("message", async (data) => {
        data = data.toString();
        if (data == "pong") {
          connection.isAlive = true;
          return;
        }

        console.log("server received  ", data);
        //terminate the connection if the first message is not hello
        if (connection.messageCount == 0 && data != "hello") {
          this.sendMessage(connection, "protocol violation", null, true);
          connection.terminate();
        } else if (data == "hello") {
          this.sendMessage(connection, null, this.ProtocolMessage.SEND_TID_SNO, false);
          connection.messageCount++;
        } else {
          //parse the string to a jsong object
          let object;
          try {
            object = JSON.parse(data);

            const error = object.error;
            const command = object.command;
            const replyData = object.data;

            //check if any error from terminal
            if (error) {
              if (command == this.ProtocolMessage.INSTALL_KEY) {
                const terminal = this.connectionMap.get(connection.serialNo);
                terminal.ACK_RECIEVED_FAILED;
              }

              logger.error(replyData);
              return;
            }

            if (command == this.ProtocolMessage.SEND_TID_SNO) {
              //extract the serial number and the teminal id
              const serialNo = replyData.serialNo;
              const terminalId = replyData.terminalId;

              const device = await this.isValidTerminal(serialNo, terminalId);
              //check if this is a registered terminal
              if (!device) {
                this.sendMessage(connection, "terminal is not registed", null, true);
                connection.terminate();
                return;
              }

              //chekc if the already connected and registered
              if (this.connectionMap.has(serialNo)) {
                this.sendMessage(
                  connection,
                  "a terminal with same serial no is already connected",
                  null,
                  true
                );
                connection.terminate();
                return;
              }

              connection.serialNo = serialNo;
              //its a valid terminal, so we create a new connection entry for that
              this.connectionMap.set(
                serialNo,
                new ClientConnection(connection, device, ConnectionState.IDLE)
              );
              logger.info("terminal connected successfully ", serialNo);
            } else if (command == this.ProtocolMessage.INSTALL_KEY) {
              console.log("key ack recieved");

              const terminal = this.connectionMap.get(connection.serialNo);
              terminal.setState(ConnectionState.ACK_RECIEVED_SUCCESS);
            }
          } catch (ex) {
            //issue parsing
            console.log(ex);
            this.sendMessage(connection, "parsing failed", null, true);
          }
        }
      });

      //remove the connection on closure
      connection.on("close", (reason) => {
        if (this.connectionMap.has(connection.serialNo))
          this.connectionMap.delete(connection.serialNo);
      });
    });
  }

  sendMessage(connection, data, command, isError) {
    const tmp = {
      error: isError,
      command: command,
      data: data,
    };

    const str = JSON.stringify(tmp);
    console.log("sending to client", str);

    isError ? logger.error(data) : logger.info(str);
    connection.send(str);
  }

  async isValidTerminal(serialNo, terminalId) {
    const { db } = require("../database/schema");
    const Device = db.Device;

    const device = await Device.findOne({ where: { SerialNo: serialNo, TerminalID: terminalId } });
    return device;
  }

  //returned the connected devices identifiers
  getConnectedTerminals() {
    const terminals = [];
    this.connectionMap.forEach((value, key) => {
      terminals.push(value.deviceObject);
    });

    return terminals;
  }

  getConnectedDevice(serial) {
    return this.connectionMap.get(serial);
  }

  sendKeyToTerminal(serialNo, keyObject) {
    const has = this.connectionMap.has(serialNo);
    if (!has) return ConnectionState.NOT_CONNECTED;

    const strKey = JSON.stringify(keyObject);
    const terminal = this.connectionMap.get(serialNo);

    if (terminal.state != ConnectionState.IDLE) return ConnectionState.BUSY;

    this.sendMessage(terminal.connection, strKey, this.ProtocolMessage.INSTALL_KEY, false);
    terminal.state = ConnectionState.WAITING_FOR_KEY_ACK;
    return terminal.state;
  }

  //implementation of ping pong protocol. to check the liveliness of the conenctios
  handle = setInterval(() => {
    this.webSocketServer.clients.forEach((client) => {
      if (!client.isAlive) {
        client.terminate();
        //remove it from the connection map
        logger.error("client seems to be dead (removing)" + client.serialNo);
        if (this.connectionMap.has(client.serialNo)) this.connectionMap.delete(client.serialNo);
      } else {
        client.isAlive = false;
        client.send("ping");
      }
    });
  }, 5000);
}

module.exports = {
  WSServer,
};
