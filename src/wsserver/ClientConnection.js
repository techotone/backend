module.exports = class ClientConnection {
  constructor(connection, device, state) {
    this.messageCounter = 0;
    this.connection = connection;
    this.deviceObject = device;
    this.state = state;
  }

  setState(state) {
    this.state = state;
  }
};
