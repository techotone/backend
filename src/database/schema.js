const sql = require("mysql2/promise");
const logger = require("../utils/logger");
const sequelize = require("sequelize");
const { hash } = require("../utils/crypto");

//define models
let Bank, Device, Key, KeyInjection, Login, Role, User, Vendor;

const dbName = "securadb";

//extract the configs
const dbHost = process.env.DB_URL || "localhost";
const dbPort = process.env.DB_PORT || 3306;
const dbRootPassword = process.env.DB_ROOT_PASSOWRD || "harshana";

const connectionConfig = {
  host: dbHost,
  port: dbPort,
  user: "root",
  password: dbRootPassword,
};

let db = {};

//helper routine
const getConnection = async () => {
  let connection;
  try {
    connection = await sql.createConnection(connectionConfig);
  } catch (error) {
    connection = null;
  }

  return connection;
};

/**
 * attempt to create connection, re-attempt creating the connection
 * unless the retries count is not exhausted
 * @param {*} conConfig
 * @param {*} retries
 */
const createConnection = async (retries, backOffInterval) => {
  return new Promise(async (resolve) => {
    console.log(connectionConfig);
    //attempt initial connection
    let connection = await getConnection();
    if (connection) return resolve(connection);

    //connection failed re attempt
    let curTry = 1;
    const handle = setInterval(async () => {
      if (curTry++ < retries) {
        logger.info("attempting to connect db " + curTry);
        connection = await getConnection();
        if (connection) {
          clearInterval(handle);
          return resolve(connection);
        }
      } else {
        clearInterval(handle);
        return resolve(null);
      }
    }, backOffInterval * 1000);
  });
};

const initializeScheme = async () => {
  //attempt to connect to the db
  let connection = await createConnection(100, 2);
  if (connection == null) {
    logger.error("connection failed to DB");
    return false;
  }

  logger.info("connected to DB");
  try {
    //create the initial DB if does not exist
    await connection.query(`CREATE DATABASE IF NOT EXISTS ${dbName}`);
    logger.info("created the initial DB");
    const seq = new sequelize(
      //create new orm object from sequelize
      dbName,
      connectionConfig.user,
      connectionConfig.password,
      {
        host: dbHost,
        dialect: "mysql",
        logging: false,
      }
    );

    logger.info("creating the tables...");
    //import the models
    Bank = require("../database/models/banks")(seq);
    Device = require("../database/models/devices")(seq);
    KeyInjection = require("../database/models/keyInjections")(seq);
    Key = require("../database/models/keys")(seq);
    Login = require("../database/models/logins")(seq);
    Role = require("../database/models/roles")(seq);
    User = require("../database/models/users")(seq);
    Vendor = require("../database/models/vendors")(seq);

    logger.info("defining the relationships..");

    //define  the resation within tables
    Vendor.hasMany(Device, { foreignkey: "VendorID", targetKey: "id" });
    Bank.hasMany(Key, { foreignkey: "BankID", targetKey: "id" });
    Bank.hasMany(User, { foreignkey: "BankID", targetKey: "id" });
    User.hasMany(Login, { foreignkey: "UserID", targetKey: "id" });
    Role.hasMany(User, { foreignkey: "RoleID", targetKey: "id" });
    User.hasMany(KeyInjection, { foreignkey: "UserID", targetKey: "id" });
    User.hasMany(Key, { foreignkey: "OwnerID", targetKey: "id" });
    Key.hasMany(KeyInjection, { foreignkey: "KeyID", targetKey: "id" });
    Device.hasMany(KeyInjection, { foreignkey: "DeviceID", targetKey: "id" });

    db.Bank = Bank;
    db.Device = Device;
    db.KeyInjection = KeyInjection;
    db.Key = Key;
    db.Login = Login;
    db.Role = Role;
    db.User = User;
    db.Vendor = Vendor;

    await seq.sync();
    await writeInitialData();
    logger.info("initial db scheme created");
  } catch (error) {
    console.log(error);
  }
};

const roleNames = ["Administrator", "Key Manager", "Operator"];

const writeInitialData = async () => {
  //we have pre defined set of roles with permissions. so we define it at init
  logger.info("writing role data");

  //check if data exist
  await writeRoles();

  //write the defult admin account
  await createDefaultAdminAccount();
};

const createDefaultAdminAccount = async () => {
  if (await isTableEmpty(User)) {
    const hashedPassword = await hash("00000000");
    const adminUser = await User.build({
      UserName: "Administrator",
      Password: hashedPassword,
      Name: "Admin",
      EMail: "admin@test.com",
      RoleId: 1,
      Owner: 1,
    });

    await adminUser.save();
  }
};

const writeRoles = async () => {
  if (await isTableEmpty(Role)) {
    roleNames.forEach(async (value) => {
      const role = await Role.build({ Role: value });
      await role.save();
    });
  }
};

const isTableEmpty = async (TableModel) => {
  let retVal = false;
  try {
    const result = await TableModel.findAll();
    if (result) {
      if (result.length > 0) retVal = false;
      else retVal = true;
    }
  } catch (er) {
    retVal = true;
  }
  return retVal;
};

module.exports.initializeScheme = initializeScheme;
module.exports.db = db;
