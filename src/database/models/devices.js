const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("Device", {
    SerialNo: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },

    TerminalID: {
      type: DataTypes.STRING(9),
      allowNull: false,
      unique: true,
    },
  });

  return model;
};
