const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("User", {
    UserName: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },

    Password: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },

    Name: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },

    EMail: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
    },

    Owner: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  return model;
};
