const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("Login", {
    SourceIP: {
      type: DataTypes.STRING(100),
    },
  });

  return model;
};
