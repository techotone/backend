const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("Key", {
    Component: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },

    Data: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
  });

  return model;
};
