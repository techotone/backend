const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("Bank", {
    Name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },
    Primary: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  });

  return model;
};
