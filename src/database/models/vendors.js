const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("Vendor", {
    Name: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
    ApplicationSecret: {
      type: DataTypes.STRING(150),
      allowNull: false,
    },
    EMail: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
    },
  });

  return model;
};
