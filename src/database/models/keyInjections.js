const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("KeyInjection", {
    InjectionState: {
      type: DataTypes.STRING,
      allowNulls: false,
    },
  });
  return model;
};
