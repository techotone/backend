const { DataTypes } = require("sequelize");

module.exports = function (seq) {
  const model = seq.define("Role", {
    Role: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
  });

  return model;
};
