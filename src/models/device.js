const joi = require("joi");

const scheme = joi.object({
  SerialNo: joi.string().required().min(4),
  TerminalID: joi.string().required().length(8),
  VendorId: joi.number().required().positive(),
});

const validate = async (object) => {
  let result = null;
  try {
    await scheme.validateAsync(object);
  } catch (error) {
    result = error;
  }

  if (result != null) result = result.details[0].message;
  return result;
};

module.exports.validate = validate;
