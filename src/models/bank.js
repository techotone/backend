const joi = require("joi");

const scheme = joi.object({
  Name: joi.string().required().max(20),
  Primary: joi.boolean().required(),
});

const validate = async (object) => {
  let result;
  try {
    await scheme.validateAsync(object);
  } catch (error) {
    result = error;
  }
  if (result != null) result = result.details[0].message;
  return result;
};

module.exports.validate = validate;
