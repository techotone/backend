const joi = require("joi");
const pwdComplexity = require("joi-password-complexity");

const scheme = joi.object({
  UserName: joi.string().required().max(20),
  Name: joi.string().required().max(20),
  EMail: joi.string().email().required().max(50),
  RoleId: joi.number().required().positive(),
  BankId: joi.number().required().positive(),
});

const pwdComplexityScehem = {
  min: 8,
  max: 30,
  lowerCase: 1,
  upperCase: 1,
  numeric: 1,
  symbol: 1,
};

const validate = async (object) => {
  let result = null;
  try {
    await scheme.validateAsync(object);
  } catch (error) {
    result = error;
  }

  if (result != null) result = result.details[0].message;
  return result;
};

module.exports.validate = validate;
