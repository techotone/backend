const joi = require("joi");

const scheme = joi.object({
  UserName: joi.string().required().max(100),
  Password: joi.string().required(),
});

const validate = async (object) => {
  let result;
  try {
    await scheme.validateAsync(object);
  } catch (error) {
    result = error;
  }
  if (result != null) result = result.details[0].message;
  return result;
};

module.exports.validate = validate;
