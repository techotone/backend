const joi = require("joi");

const scheme = joi.object({
  Component: joi.string().valid("COMP 1", "COMP 2").required(),
  Data: joi.string().length(32).required(),
});

const validate = async (object) => {
  let result;
  try {
    await scheme.validateAsync(object);
  } catch (error) {
    result = error;
  }

  if (result != null) result = result.details[0].message;
  return result;
};

module.exports.validate = validate;
