const express = require("express");
const auth = require("../middleware/auth");
const permission = require("../middleware/permission");

module.exports = function (app) {
  app.use(express.json());
  app.use(auth);
  app.use(permission);
};
