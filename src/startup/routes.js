const user = require("../routes/users");
const device = require("../routes/devices");
const key = require("../routes/keys");
const bank = require("../routes/banks");
const login = require("../routes/login");
const vendor = require("../routes/vendors");
const keyInections = require("../routes/keyInjetions");

module.exports = function (app) {
  app.use("/api/vi/user", user);
  app.use("/api/vi/device", device);
  app.use("/api/vi/key", key);
  app.use("/api/vi/bank", bank);
  app.use("/api/vi/login", login);
  app.use("/api/vi/vendor", vendor);
  app.use("/api/vi/keyinjection", keyInections);
};
