const StatusCodes = Object.freeze({
  OK: 200,
  SERVER_ERROR: 500,
  BAD_REQUEST: 400,
  CONFLICT: 409,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  NOT_ALLOWED: 405,
  NOT_AUTHORIZED: 401,
});
module.exports = StatusCodes;
