const bcrypt = require("bcrypt");
const crypto = require("crypto");

const hash = async (data) => {
  let result;
  try {
    result = await bcrypt.hash(data, 10);
  } catch (error) {
    result = error;
  }

  return result;
};

const generateRandom = (len) => {
  let randString = "";
  while (randString.length < len) {
    randString += Math.floor(Math.random() * 10);
  }

  return randString;
};

const ENC_KEY_LEN = 16;

const deriveKeyFromAdminLogin = async (User) => {
  //for password we combine primary administrators username and password
  const primAdminUser = await User.findOne({ wherer: { id: 1 } });
  if (!primAdminUser) throw new ServerError();

  const secret = primAdminUser.dataValues.UserName + primAdminUser.dataValues.Password;

  let ret;
  try {
    const key = crypto.pbkdf2Sync(secret, "salt", 100, ENC_KEY_LEN, "sha512");
    ret = key.toString("hex");
  } catch (error) {
    ret = null;
  }

  return ret;
};

const iv = Buffer.from("0000000000000000");

const encryptAESHEX = (key, clearData) => {
  let cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
  let encrypted = cipher.update(clearData);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  encrypted = encrypted.toString("hex");
  return encrypted;
};

const decryptAESHEX = (key, encryptedData) => {
  let decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
  let decrypted = decipher.update(Buffer.from(encryptedData, "hex"));
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};

const xorHexString = (left, right) => {
  if (left.length != right.length) return null;

  let str = "";
  for (i = 0; i <= left.length - 2; i += 2) {
    const l = left.substr(i, 2);
    const r = right.substr(i, 2);

    const tmpVal = parseInt(l, 16) ^ parseInt(r, 16);

    str += tmpVal.toString(16);
  }

  return str;
};

module.exports.hash = hash;
module.exports.generateRandom = generateRandom;
module.exports.deriveKeyFromAdminLogin = deriveKeyFromAdminLogin;
module.exports.encryptAESHEX = encryptAESHEX;
module.exports.decryptAESHEX = decryptAESHEX;
module.exports.xorHexString = xorHexString;
