const { Forbidden, NotAllowed } = require("../error/BaseError");
const { handleError } = require("../error/errorHandler");
const logger = require("../utils/logger");

const debug = true;
module.exports = async function (req, res, next) {
  const path = req.path;
  const method = req.method.toLowerCase();

  if (!req.auth) return next();

  if (path.includes("login")) {
    next();
    return;
  }

  console.log("validation permissions...");
  const role = req.user.role;

  const adminRole = "Administrator";
  const keyMangerRole = "Key Manager";
  const operatorRole = "Operator";

  if (debug) {
    logger.info("path " + path);
    logger.info("role " + role);
    logger.info("method " + method);
  }

  //get the associated bank of the user

  try {
    if (path.includes("bank")) {
      //accessing bank resource
      if (method == "post" || method == "put" || method == "delete") {
        //attempting to create a resource
        if (role != adminRole)
          throw new Forbidden("you are not allowed to create, update or delete banks");
      }
      next();
    } else if (path.includes("device")) {
      if (method == "post" || method == "put" || method == "delete") {
        if (role != operatorRole)
          throw new Forbidden("you are not allowed to create, update or delete devices");
      }
      next();
    } else if (path.includes("user")) {
      if (method == "post" || method == "put" || method == "delete") {
        if (role != adminRole)
          throw new Forbidden("you are not allowed to create, update, delete  users");
      }
      next();
    } else if (path.includes("keyinjection")) {
      // if (method == "post" || method == "put" || method == "delete" || method == "get") {
      //   if (role != keyMangerRole)
      //     throw new Forbidden("you are not allowed to create, update, delete or view keys ");
      // }

      next();
    } else if (path.endsWith("inject")) {
      // if (method == "post" || method == "put" || method == "delete" || method == "get") {
      //   if (role != keyMangerRole)
      //     throw new Forbidden("you are not allowed to create, update, delete or view keys ");
      // }

      next();
    } else if (path.includes("key")) {
      if (method == "post" || method == "put" || method == "delete" || method == "get") {
        if (role != keyMangerRole)
          throw new Forbidden("you are not allowed to create, update, delete or view keys ");
      }

      next();
    } else if (path.includes("vendor")) {
      if (method == "post" || method == "put" || method == "delete") {
        if (role != adminRole)
          throw new Forbidden("you are not allowed to create, update, delete or view vendors ");
      }
      next();
    }
  } catch (error) {
    handleError(error, res);
  }
};
