const { BadRequest, NotAllowed } = require("../error/BaseError");
const { handleError } = require("../error/errorHandler");
const { db } = require("../database/schema");
const config = require("config");
const jtoken = require("jsonwebtoken");

const auth = true;
const tokenKey = config.get("tokenKey");

const User = db.User;

module.exports = async function (req, res, next) {
  req.auth = auth;
  if (req.path.includes("login")) return next();

  if (!auth) {
    //debuggin and testing the applcation so we set the user as admin
    const user = await User.findOne({ where: { id: 1 } });
    user.userID = user.id;
    req.user = user;
    return next();
  }

  //verify the web token
  try {
    const token = req.headers["x-auth-token"];
    if (!token) throw new BadRequest("auth token is missing");

    //verify it
    try {
      jtoken.verify(token, tokenKey);
    } catch (error) {
      throw new NotAllowed("Token Error");
    }

    let user = jtoken.decode(token);
    delete user.iat;
    delete user.exp;
    req.user = user;

    //token is valid
    next();
  } catch (error) {
    handleError(error, res);
  }
};
